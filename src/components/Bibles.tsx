import React from "react"
import {Bible} from "../types/Bible"
import BibleView from "./BibleView"

const Bibles = ({bibles, onBibleSelect, goTo}: { bibles: Bible[], onBibleSelect: any, goTo: any }) => {
    return (
        <>
            {bibles.map((bible: Bible) => (
                <BibleView
                    key={bible.version}
                    bible={bible}
                onBibleSelect={()=>onBibleSelect(bible)}
                goTo={goTo}/>)
            )}
        </>
    )
}

export default Bibles
