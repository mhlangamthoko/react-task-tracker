import React, {forwardRef, useEffect, useState} from 'react'
import {Badge, Button, Card} from 'react-bootstrap'
import {Book} from '../types/Book'
import {findByBibleIdAndIndex} from './Util';
import {useLocation, useNavigate, useParams} from "react-router-dom";
import {MDBCard, MDBCardBody} from "mdb-react-ui-kit";

export const ChapterSelector = forwardRef(({onChapterSelect}: {
    onChapterSelect: any
}, ref) => {

    const {id, bookIndex} = useParams()
    const perLine = 8;
    const [title, setTitle] = useState<string>('');
    const [contents, setContents] = useState<any>(<></>);
    const [summary, setSummary] = useState<string>('');
    const loc = useLocation()

    useEffect(() => {
        const setupChapterButtons = (bk: Book) => {
            // load book then do this
            var group = -1;
            const groups: number[][] = []
            for (let index = 0; index < bk.totalChapters; index++) {
                if (index % perLine === 0) {
                    groups.push([])
                    group = group + 1;
                }
                groups[group].push(index + 1)
            }
            setContents(getContents(groups))
        }
        findByBibleIdAndIndex(id ? id.toString() : "", bookIndex ? bookIndex.toString() : "")
            .then(response => {
                return response.json()
            })
            .then(data => {
                setSummary(`This book contains ${data.totalChapters} chapters`)
                setTitle(data.name)
                setupChapterButtons(data)
            })
            .catch(error => {
                console.log(error);
            })
    }, [title])

    const nav = useNavigate();
    const loadVerses = (chapterNo: number) => {
        onChapterSelect(chapterNo)
        nav(`${loc.pathname}/${chapterNo}`)
    }

    const getKey = (chapterGroups: number[][], group: number[]) => {
        return chapterGroups.findIndex(g => g === group)
    }

    const getContents: any = (chapterGroups: number[][]) => {
        return chapterGroups.map(group =>
            (
                <div key={getKey(chapterGroups, group)}>
                    {group.map(chapterNo => (
                        <span key={chapterNo}>
                        <Badge as={Button} pill bg="secondary" style={{width: '35px'}}
                               onClick={(e) => loadVerses(chapterNo)}>{chapterNo}</Badge>{' '}
                    </span>
                    ))}
                </div>
            )
        )
    }

    return (
        <>
            <MDBCard style={{borderRadius: '5px', backgroundColor: 'lightgray'}}>
                <MDBCardBody>
                    <Card.Title>{title} {' '}</Card.Title>
                    <Card.Text>
                        {summary}
                    </Card.Text>

                    {
                        contents
                    }
                    <p></p>
                </MDBCardBody>
            </MDBCard>
        </>
    )
});
