import React, {useEffect, useState} from 'react'
import {Button, Col, FloatingLabel, Form, Row} from 'react-bootstrap'
import {useParams} from 'react-router-dom'
import {Scripture} from '../types/Scripture'
import ScriptureView from './ScriptureView'
import {fetchVersionById, fetchVersions, login, searchVerses, searchVersesByVersion} from './Util'

const Search = () => {

    const [text, setText] = useState<string>('')
    const [selectedVersion, setSelectedVersion] = useState<string>('All versions')
    const [versions, setVersions] = useState<string[]>([])
    const [scriptures, setScriptures] = useState<Scripture[]>([])
    const [searching, setSearching] = useState<boolean>(false)
    const [loading, setLoading] = useState<boolean>(true)

    const {id} = useParams()

    const submit = (e: any) => {
        e.preventDefault()
        setSearching(true)
        var promise;
        if (selectedVersion === 'All versions') {
            promise = searchVerses(text);
        } else {
            promise = searchVersesByVersion(selectedVersion, text);
        }
        promise
            .then(response => response.json())
            .then(data => {
                const scriptures: Scripture[] = data
                setScriptures(scriptures)
                setSearching(false)
            }).catch(e => {
            setSearching(false)
            //todo: error handling
            console.log(e);
            alert('error encountered')
        })
    }

    useEffect(() => {
        const loadVersions = () => {
            if (id) {
                login({id: 1, username: "admin", password: "password", roles: "USER"})
                    .then(response => {
                        const headers = response.headers.get("set-cookie")
                        console.log(headers);
                        response.headers.forEach(header => {
                            console.log(header);
                        });

                        return response.json()
                    })
                    .then(data => {
                        console.log(data);
                    })
                    .catch(error => {
                        console.log(error);
                    })

                fetchVersionById(parseInt(id))
                    .then(response => response.json())
                    .then(data => {
                        const version: string = data
                        setVersions([version])
                        setSelectedVersion(version)
                        setLoading(false)
                    })
            } else {
                fetchVersions()
                    .then(response => response.json())
                    .then(data => {
                        const versions: string[] = data
                        setVersions(versions)
                        setLoading(false)
                    })
            }
        }
        loadVersions()
    }, [id])

    return loading ? (<div>
            <h1>Please wait ... </h1>
        </div>)
        : (
            <Form onSubmit={(e) => submit(e)}>
                <Row className="g-2">
                    <Col md>
                        <FloatingLabel controlId="floatingSelectGrid" label="Select version">
                            <Form.Select aria-label="Floating label select example" onChange={e => {
                                setSelectedVersion(e.target.value)
                            }}>
                                <option value="All versions">All versions</option>
                                {
                                    versions.map(version => (
                                        <option
                                            key={version}
                                            value={version}>{version}</option>
                                    ))
                                }
                            </Form.Select>
                        </FloatingLabel>
                    </Col>
                    <Col md>
                        <FloatingLabel controlId="floatingInputGrid" label="Search text">
                            <Form.Control type="text" placeholder="" value={text}
                                          onChange={(e) => setText(e.target.value)}/>
                        </FloatingLabel>
                    </Col>
                    <Button type='submit'>Search</Button>
                </Row>
                <Row>
                    <p></p>
                    <p></p>
                    <ScriptureView scriptures={scriptures}/>
                </Row>
            </Form>
        )
}

export default Search
