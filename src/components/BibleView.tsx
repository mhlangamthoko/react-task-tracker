import React from 'react'
import {FaSearch} from 'react-icons/fa'
import {useNavigate} from 'react-router-dom'
import {Bible} from '../types/Bible'


const BibleView = ({bible, onBibleSelect, goTo}: { bible: Bible, onBibleSelect: any, goTo: any }) => {

    const navigator = useNavigate()

    const navigate = (url: string, crumbs?:string[]) => {
        onBibleSelect(bible.id, bible.version)
        navigator(url)
        if (crumbs) {
            goTo(url, crumbs)
        }
    }

    return (
        <div className={`task reminder`}>
            <h3>
                <span onClick={() => navigate(`/bible/${bible.id}/read`)}>{bible.version}</span>
                <span>
                    <FaSearch style={{color: 'skyblue', cursor: 'pointer'}}
                              onClick={(e) => {

                                  e.preventDefault()
                                  navigate(`/search/${bible.id}`, [])
                              }}
                    />
                </span>
            </h3>
        </div>
    )
}

export default BibleView
