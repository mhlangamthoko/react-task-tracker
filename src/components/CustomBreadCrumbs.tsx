import React, {forwardRef, useEffect, useImperativeHandle, useState} from "react";
import {Breadcrumb} from "react-bootstrap";
import {useLocation, useNavigate} from "react-router-dom";
import {Book} from "../types/Book";
import {Bible} from "../types/Bible";
import {fetchVersionById, findByBibleIdAndIndex} from "./Util";
import {MDBCard, MDBCardBody} from "mdb-react-ui-kit";
import eventEmitter from '../Event';

const CustomBreadCrumbs = forwardRef(({onAction}: { onAction: any }, ref) => {

    const nav = useNavigate()
    const location = useLocation()
    const [breadCrumbs, setBreadCrumbs] = useState<string[]>(["Home"]);
    const [hidden, setHidden] = useState<boolean>(false);


    const navigateTo = (index: number, active: boolean) => {
        alert("Emitting")
        eventEmitter.emit('crumbs', 'Hi')
        if (active) {
            return;
        }
        setHidden(false)
        if (index === 0) {
            setBreadCrumbs([breadCrumbs[0]])
            nav(`/`)
            return;
        }
        setBreadCrumbs(breadCrumbs.slice(0, index + 1))
        const strings = location.pathname.split("/");
        console.log(strings)
        switch (index) {
            case 1:
                nav(`/bible/${strings[2]}/read`)
                break;
            case 2:
                console.log(breadCrumbs)
                const addr = `/bible/${strings[2]}/read/${strings[4]}`
                nav(addr)
                break;
        }
    };

    const loadPage = () => {
        let path = location.pathname;
        if (path.match("^/$")) {
            console.log(breadCrumbs[0])
            setBreadCrumbs(["Home"])
        } else if (path.match("^/bible/\\d+/read$")) {
            console.log(breadCrumbs)
            const id = parseInt(path.split("/")[2])
            fetchVersionById(id)
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                    setBreadCrumbs(["Home", data])
                })
        } else if (path.match("^/bible/\\d+/read/\\d+$")) {
            console.log(breadCrumbs)
            const id = parseInt(path.split("/")[2])
            const bookIndex = path.split("/")[4]
            fetchVersionById(id)
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                    findByBibleIdAndIndex(id.toString(), bookIndex)
                        .then(response => response.json())
                        .then(book => {
                            console.log(book)
                            setBreadCrumbs(["Home", data, book.name])
                        })
                })
        } else if (path.match("^/bible/\\d+/read/\\d+/\\d+$")) {
            console.log(breadCrumbs)
            const id = parseInt(path.split("/")[2])
            const bookIndex = path.split("/")[4]
            const chapterNo = path.split("/")[5]
            fetchVersionById(id)
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                    findByBibleIdAndIndex(id.toString(), bookIndex)
                        .then(response => response.json())
                        .then(book => {
                            console.log(book)
                            setBreadCrumbs(["Home", data, book.name, chapterNo])
                        })
                })
        }
    }

    useImperativeHandle(ref, () => ({
        navigateTo(value: string) {
            setBreadCrumbs([...breadCrumbs, value])
        },
        selectBible(bible: Bible) {
            const newVal = ['Home', bible.version]
            setBreadCrumbs(newVal)
            console.log(newVal)
            nav(`/bible/${location.pathname.split('/')[2]}/read`)
        },
        selectBook(book: Book) {
            setBreadCrumbs([...breadCrumbs, book.name])
            console.log(location.pathname.split('/'))
            nav(`/bible/${location.pathname.split('/')[2]}/read/${book.index}`)
        },
        goTo(url: string, crumbs: string[]) {
            setBreadCrumbs(crumbs)
            nav(url)
        },
        handlePageLoad() {
            loadPage();
        },
        hide() {
            setHidden(true)
        },
        show() {
            setHidden(false)
        }
    }), [breadCrumbs])

    useEffect(() => {
        loadPage()
    }, [hidden]);

    return (
        breadCrumbs.length > 0 ?
            <MDBCard style={{borderRadius: '5px'}} hidden={hidden}>
                <MDBCardBody>
                    <Breadcrumb>
                        {
                            breadCrumbs.map((value, index) => (
                                <Breadcrumb.Item
                                    key={value}
                                    active={index !== 0 && index === breadCrumbs.length - 1}
                                    onClick={() => navigateTo(index, index === breadCrumbs.length - 1)}>
                                    {value}
                                </Breadcrumb.Item>
                            ))
                        }
                    </Breadcrumb>
                </MDBCardBody>
            </MDBCard>
            : <></>
    )
})
export default CustomBreadCrumbs;