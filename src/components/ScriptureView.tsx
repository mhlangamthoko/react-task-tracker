import React from 'react'
import {Accordion} from 'react-bootstrap'
import {Scripture} from '../types/Scripture'

const ScriptureView = ({scriptures}: { scriptures: Scripture[] }) => {
    return (
        <Accordion defaultActiveKey="0">
            {scriptures.map(scripture => (
                <Accordion.Item key={`${scripture.version}${scripture.bookName}${scripture.chapterNo}`} eventKey={`${scripture.version}${scripture.bookName}${scripture.chapterNo}`}>
                    <Accordion.Header>{`${scripture.version} -> ${scripture.bookName} ${scripture.chapterNo}`}</Accordion.Header>
                    <Accordion.Body>
                        {
                            scripture.verses.map(verse => (
                                <p key={verse.number}>
                                    {
                                        `${verse.number}) ${verse.text}`
                                    }
                                </p>
                            ))
                        }
                    </Accordion.Body>
                </Accordion.Item>
            ))}
        </Accordion>
    )
}

export default ScriptureView
