import React, {useEffect, useState} from 'react'
import {Card} from 'react-bootstrap'
import {Book} from '../types/Book'
import {Chapter} from '../types/Chapter';
import {Verse} from '../types/Verse';
import {findByBibleIdAndIndex, findVersesByBibleIdBookIdAndChapterNo} from './Util';
import {useParams} from "react-router-dom";
import {MDBCard, MDBCardBody} from "mdb-react-ui-kit";

export const ChapterVersesView = () => {

    const {id, bookIndex, chapterNo} = useParams()
    const [title, setTitle] = useState<string>('');
    const [contents, setContents] = useState<any>(<></>);

    useEffect(() => {

        loadVerses(parseInt(chapterNo ? chapterNo : "1"))
    }, [])

    const loadVerses = (chapterNo: number) => {
        const bibleId = id ? parseInt(id) : 1;
        let bookIndex1 = bookIndex ? parseInt(bookIndex) : 1;
        findByBibleIdAndIndex(bibleId.toString(), bookIndex1.toString())
            .then(response => {
                console.log(response)
                if (response.ok) {
                    return response.json()
                }
                return {"error": "error"}
            })
            .then(data => {
                if (data.error) {
                    console.log("some error", data)
                    return
                }
                const book: Book = data;
                findVersesByBibleIdBookIdAndChapterNo(bibleId, bookIndex1, chapterNo)
                    .then(response => response.json())
                    .then(data => {
                        const chapter: Chapter = data;
                        setTitle(book.name + ' ' + chapter.number + `: 1 - ${chapter.totalVerses}`)
                        setContents(chapterContents(chapter))
                    })
            })
            .catch(reason => {
                console.log(reason)
            })
    }

    const chapterContents: any = (chapter: Chapter) => {
        return <>
            {
                chapter.verses.map((verse: Verse) => (
                    <p key={verse.number}>{`${verse.number}) ${verse.text}`}</p>
                ))
            }
        </>
    }

    return (
        <>
            <MDBCard className={`mb-3 overflow-auto`} style={{height:'600px',borderRadius: '5px', backgroundColor: 'lightgray'}}>
                <MDBCardBody>
                    <Card.Title className={`p-3`}>{title} {' '}</Card.Title>
                    <span>{contents}</span>
                </MDBCardBody>
            </MDBCard>
        </>
    )
};
