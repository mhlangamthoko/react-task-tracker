import React, {useEffect, useState} from 'react'
import {Col, Dropdown, Form, Row} from 'react-bootstrap'
import {useParams} from 'react-router-dom'
import {findBibleById} from './Util'
import CustomMenu from './CustomMenu'
import CustomToggle from './CustomToggle'
import {Book} from '../types/Book'
import {Bible} from '../types/Bible'
import {MDBCard, MDBCardBody} from 'mdb-react-ui-kit'

const BookSelector = ({onBookSelect}: { onBookSelect: any }) => {

    const {id} = useParams()
    const [bible, setBible] = useState<Bible>({
        id: 0,
        language: '',
        version: '',
        books: []
    });

    useEffect(() => {
        const load = () => {
            if (id) {
                findBibleById(parseInt(id))
                    .then(response => {
                        console.log(response)
                        return response.json()
                    })
                    .then(bibleResult => {
                        console.log(bibleResult)
                        if (bibleResult) {
                            const bible: Bible = bibleResult
                            setBible(bible)
                        }
                    })
                    .catch(reason => {
                        console.log(reason)
                    })
            }
        }
        load()
    }, [])

    const selectBook = (book: Book) => {
        onBookSelect(book)
    }

    return (
        <Form onSubmit={(e) => e.preventDefault()}>

            <MDBCard style={{borderRadius: '5px', backgroundColor:'lightgray'}}>
                <MDBCardBody>
                    <Row>
                        <Col>
                            <p></p>
                            <h3>{bible.version}</h3>
                            <Dropdown>
                                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                                    Select book
                                </Dropdown.Toggle>
                                <Dropdown.Menu as={CustomMenu}>
                                    {bible.books.map((book) =>
                                        <Dropdown.Item
                                            key={`${book.id}`}
                                            eventKey={`${book.id}`}
                                            onClick={() => {
                                                selectBook(book)
                                            }}>
                                            {book.name}
                                        </Dropdown.Item>
                                    )}
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                    </Row>
                </MDBCardBody>
            </MDBCard>
        </Form>
    )
}

export default BookSelector
