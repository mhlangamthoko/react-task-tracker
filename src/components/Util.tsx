import User from "../types/User"
import {Buffer} from 'buffer';

export const baseUrl = process.env.REACT_APP_BASE_URL
export const defaultMapping = 'bible'
export const bible = 'bible'
export const book = 'books'
export const chapter = 'chapter'
export const verse = 'verse'
export const POST = 'POST'
export const GET = 'GET'
const headers = {
    "Content-type": "application/text"
}

export const search = (text?: string) => {
    return fetch(`${baseUrl}/${bible}/search`, {
        method: POST,
        headers: headers,
        body: text
    })
}

export const fetchBibles = () => {
    return fetch(`${baseUrl}/${bible}`)
}

export const fetchVersionById = (id: number) => {
    console.log("Version by id: " + id)
    return fetch(`${baseUrl}/${bible}/versions/${id}`)
}

export const login = (user: User) => {
    const encodedString = Buffer.from(`${user.username}:${user.password}`).toString('base64');
    console.log(encodedString);

    return fetch(`${baseUrl}/user/current`
        , {
            method: POST,
            headers: {
                "Authorization": `Basic ${encodedString}`,
            },
            body: ""
        }
    )
}

export const fetchVersions = () => {
    return fetch(`${baseUrl}/${bible}/versions`)
}

export const fetchVerses = (bookId: number, chapterNo: number) => {
    return fetch(`${baseUrl}/${chapter}/bookId/${bookId}/${chapterNo}`)
}

export const findVersesByBibleIdBookIdAndChapterNo = (bibleId: number, bookIndex: number, chapterNo: number) => {
    return fetch(`${baseUrl}/${chapter}/bibleId/${bibleId}/bookIndex/${bookIndex}/${chapterNo}`)
}

export const searchVerses = (text: string) => {
    return fetch(`${baseUrl}/${verse}/search`, {
        method: 'POST',
        headers: headers,
        body: text
    })
}

export const searchVersesByVersion = (version: string, text: string) => {
    return fetch(`${baseUrl}/${verse}/search/${version}`, {
        method: 'POST',
        headers: headers,
        body: text
    })
}

export const findBibleById = (id: number) => {
    return fetch(`${baseUrl}/${bible}/${id}`)
}

export const findByBibleIdAndIndex = (bibleId: string, bookIndex: string) => {
    return fetch(`${baseUrl}/${book}/bibleId/${bibleId}/bookIndex/${bookIndex}`)
}