import React, {useEffect, useState} from 'react'
import {Nav, Navbar} from 'react-bootstrap'
import {Link, useLocation} from 'react-router-dom'
import logo from '../logo.svg'

const TopNav = ({goTo}: { goTo: any }) => {

    const [navIn, setNavIn] = useState(false)
    const location = useLocation()

    const collapseNav = (e: any) => {
        const togglerBtn = document.getElementsByClassName('navbar-toggler')[0]
        const display = getComputedStyle(togglerBtn).display
        if (display === 'none') {
            return
        }
        setNavIn(!navIn)
    }

    useEffect(() => {
        if (location.pathname === "/search" || location.pathname.match("/search/(\\d+)?")) {
            goTo(`/search`, [])
        }
    }, [])

    return (
        <Navbar
            style={{paddingLeft: "10px", paddingRight: "10px"}}
            bg='dark'
            variant='dark'
            sticky='top'
            expand='lg' collapseOnSelect>
            <Link to={'/'} className={'navbar-brand'} role='button'
                  onClick={(e) => {
                      e.preventDefault()
                      goTo(`/`, [`Home`])
                  }}>
                <img src={logo} alt="" width='40px' height='40px'/>{' '}
                Smart Bible
            </Link>
            <Navbar.Toggle onClick={collapseNav}/>
            <Navbar.Collapse in={navIn}>
                <Nav>
                    <Link className='nav-link' to={"/bibles"} onClick={(e) => {
                        e.preventDefault()
                        collapseNav(e)
                        goTo(`/`, [`Home`])
                    }}>Bibles</Link>
                    <Link className='nav-link' to={"/search"} onClick={(e) => {
                        e.preventDefault()
                        collapseNav(e)
                        goTo(`/search`, [])
                    }}>Search</Link>
                    {
                        /* 
                        <NavDropdown title='Levels'>
                            <NavDropdown.Item href="#full">Primary</NavDropdown.Item>
                            <NavDropdown.Item href="#full">Senior Primary</NavDropdown.Item>
                            <NavDropdown.Item href="#full">Combined Primary</NavDropdown.Item>
                            <NavDropdown.Item href="#full">Secondary</NavDropdown.Item>
                            <NavDropdown.Item href="#full">Combined Secondary</NavDropdown.Item>
                        </NavDropdown>
                        */
                        /*
                        <Link className='nav-link' to={"/link1"} onClick={collapseNav}>Link 1</Link>
                        <Link className='nav-link' to={"/contact-us"} onClick={collapseNav}>Contact Us</Link> 
                        */
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default TopNav