import React, {useState} from "react";
import {FormControl} from "react-bootstrap";

// Dropdown needs access to the DOM of the Menu to measure it
const CustomMenu = React.forwardRef(
    ({children, style, className, 'aria-labelledby': labeledBy}: any, ref: any) => {
        const [value, setValue] = useState('');
        if (!className.includes('show')) {
            if (value !== '') {
                setValue('')
            }
        }

        return (
            <div
                ref={ref}
                style={style}
                className={className}
                aria-labelledby={labeledBy}
            >
                <FormControl
                    autoFocus
                    className="mx-3 my-2 w-auto"
                    placeholder="Type to filter..."
                    onChange={(e) => setValue(e.target.value)}
                    value={value}
                />
                <ul className="list-unstyled">
                    {React.Children.toArray(children).filter(
                        (child: any) =>
                            !value || child.props.children.toLowerCase().includes(value.toLowerCase()),
                    )}
                </ul>
            </div>
        );
    },
);

export default CustomMenu