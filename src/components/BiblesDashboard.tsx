import React, {useEffect, useState} from 'react'
import Header from "./Header";
import Bibles from "./Bibles";
import {fetchBibles} from './Util';
import {Bible} from '../types/Bible';
import {MDBCard, MDBCardBody} from "mdb-react-ui-kit";

const BiblesDashboard = ({onBibleSelect, goTo}: { onBibleSelect: any, goTo: any }) => {

    const [bibles, setBibles] = useState<Bible[]>([])
    const [loading, setLoading] = useState<boolean>(true)

    useEffect(() => {
        const getBibles = () => {
            fetchBibles()
                .then(response => response.json())
                .then(data => {
                    setBibles(data)
                    setLoading(false)
                })
        }

        getBibles()
    }, [])
    return (
        <>

            <MDBCard style={{borderRadius: '5px', backgroundColor: 'lightgray'}}>
                <MDBCardBody>
                    <Header
                        title='Bibles'/>
                    {
                        loading ? "Please wait, loading bibles"
                            :
                            (
                                bibles.length > 0 ?
                                    <Bibles bibles={bibles} onBibleSelect={onBibleSelect} goTo={goTo}/>
                                    :
                                    ('No bibles available at the moment, please try again later')
                            )
                    }
                </MDBCardBody>
            </MDBCard>
        </>
    )
}

export default BiblesDashboard
