import React from "react";

const CustomToggle = React.forwardRef(({children, onClick}: { children: any, onClick: any }, ref: any) => (
    <a
        className="btn btn-primary"
        href="val"
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        {children}
        &#x25bc;
    </a>
));

export default CustomToggle