import 'bootstrap/dist/css/bootstrap.css'
import './App.css'
import TopNav from "./components/TopNav";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Search from "./components/Search";
import BiblesDashboard from './components/BiblesDashboard';
import {Container} from 'react-bootstrap';
import ContactUs from './components/ContactUs';
import React, {useEffect, useRef} from "react";
import CustomBreadCrumbs from "./components/CustomBreadCrumbs";
import BookSelector from "./components/BookSelector";
import {Book} from "./types/Book";
import {ChapterSelector} from "./components/ChapterSelector";
import {ChapterVersesView} from "./components/ChapterVersesView";
import {Bible} from "./types/Bible";
import eventEmitter from './Event';

const App = () => {

    const ref = useRef(null)

    useEffect(() => {
        // @ts-ignore
        ref.current.handlePageLoad()
        eventEmitter.addListener('crumbs', (args: any) => {
            console.log("Some crumbs: ", args)
            console.log("Listeners: ", eventEmitter.eventNames())

        })
    }, []);


    const f = () => {
        alert("What up")
    }

    const navigateTo = (path: string) => {
        // @ts-ignore
        ref.current.navigateTo(path)
    }

    const onBookSelect = (book: Book) => {
        // @ts-ignore
        ref.current.selectBook(book)

    };

    const onBibleSelect = (bible: Bible) => {
        // @ts-ignore
        ref.current.selectBible(bible)

    };
    const onChapterSelect = (chapterNo: number) => {
        navigateTo(chapterNo.toString())
    };
    const goTo = (url: string, crumbs: string[]) => {
        // @ts-ignore
        ref.current.goTo(url, crumbs)
    };
    return (
        <Router>
            <div className="App">
                <TopNav goTo={(url: string, crumbs: string[]) => {
                    // @ts-ignore
                    ref.current.goTo(url, crumbs)
                }}/>
                <Container style={{paddingTop: '30px'}}>
                    <CustomBreadCrumbs
                        onAction={f}
                        ref={ref}/>
                </Container>
                <Container style={{paddingTop: '30px'}}>
                    <Routes>
                        <Route path='/' element={<BiblesDashboard onBibleSelect={onBibleSelect} goTo={goTo}/>}/>
                        <Route path='/search' element={<Search/>}/>
                        <Route path='/search/:id' element={<Search/>}/>
                        <Route path='/bibles' element={<BiblesDashboard onBibleSelect={onBibleSelect} goTo={goTo}/>}/>
                        <Route path='/bible/:id/read' element={<BookSelector onBookSelect={onBookSelect}/>}/>
                        <Route path='/bible/:id/read/:bookIndex'
                               element={<ChapterSelector
                                   onChapterSelect={onChapterSelect}/>
                               }/>
                        <Route path='/bible/:id/read/:bookIndex/:chapterNo' element={<ChapterVersesView/>}/>
                        <Route path='/contact-us' element={<ContactUs/>}/>
                    </Routes>
                </Container>
            </div>
        </Router>
    );
}

export default App;
