import {Book} from "./Book";

export interface Bible {
    id: number

    language: string

    version: string

    books: Book[]
}