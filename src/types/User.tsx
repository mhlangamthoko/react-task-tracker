type User = {
    id: number,
    username: string,
    password: string,
    roles: string,
};

export default User