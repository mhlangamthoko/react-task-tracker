import {Verse} from "./Verse";

export interface Chapter {
    id: number

    bookIndex: number

    number: number

    totalVerses: number

    verses: Verse[]
}
