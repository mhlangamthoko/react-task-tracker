export interface Verse {
    id: number

    number: number

    text: string
}