import {Verse} from "./Verse";

export interface Scripture {
    version: string

    bookIndex: number

    bookName: string

    chapterNo: number

    verses: Verse[]
}
