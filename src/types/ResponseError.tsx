type ResponseError = {
    timestamp: number,
    status: number,
    error: string,
    path: string,
};

export default ResponseError