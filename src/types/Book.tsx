export interface Book {
    id: number

    index: number

    name: string

    totalChapters: number
}